﻿<?php

/** 
* ==  Vx_Download  == 
*
*文件名：
*		sql_setup.php
* 
*文件功能： 
*		数据库创建
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*
*/

// 设置编码
header("Content-type:text/html;charset=utf-8");


// 判断数据库是否已创建
if(!file_exists("sql.lock")){
	// 如果没有，则进行连接创建数据库，
	include('config.php');	 

	// 使用 sql 创建数据表vx_download_share
	$sql = "CREATE TABLE vx_download_share (
	url int(10) DEFAULT NULL COMMENT '文章链接',
	title 	longtext NOT NULL COMMENT '文章标题',
	ali_title longtext NOT NULL COMMENT '分享标题',
	ali_share longtext NOT NULL COMMENT '阿里链接',
	code longtext NOT NULL COMMENT '提取码',
	other_title longtext NOT NULL COMMENT '其他标题',
	other_share longtext NOT NULL COMMENT '其他分享',
	uv_visitor int(10) DEFAULT NULL COMMENT '浏览量'
	)";

	$sql_user = "CREATE TABLE vx_download_user (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY COMMENT 'id', 
	username VARCHAR(30) NOT NULL COMMENT '用户名',
	password VARCHAR(30) NOT NULL COMMENT '密码',
	email VARCHAR(50) COMMENT '邮箱',
	reg_date TIMESTAMP COMMENT '注册时间',
	usertype int(10) DEFAULT NULL COMMENT '用户类型',
	vxia_net longtext NOT NULL COMMENT '微夏博客网'
	)";

	$q="insert into vx_download_user(id,username,password,usertype,email,vxia_net) values (0,'".USERNAME."','".PASSWORD."',1,'".USER_MAIL."','http://www.vxia.net')";


	if (mysqli_query($conn, $sql) & mysqli_query($conn, $sql_user) & mysqli_query($conn, $q)) {
		echo "数据表 MyGuests 创建成功";
	} else {
		echo "创建数据表错误: " . mysqli_error($conn);
	}
	$myfile = fopen("sql.lock", "w") or die("写入文件失败！");
	$txt = "Vx_Download SQL写入成功！微夏博客网 vxia.net";
	fwrite($myfile, $txt);
	fclose($myfile);

mysqli_close($conn);


}else{
	echo "非法操作！";
}



?>