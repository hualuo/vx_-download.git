<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		删除相关数据
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

//  启动 Session
session_start();

// 加载所需配置文件
require_once('../config.php');

//  判断是否登陆
if (empty($_SESSION['username'])){
	header('location:./login.php');
}

// 获取传入的用户ID
$url = $_GET['url'];

//删除指定数据  
mysqli_query($conn,"DELETE FROM vx_download_share WHERE url={$url}") or die('删除数据出错：'.mysql_error()); 

// 删除完跳转到用户管理界面
header("Location:index.php"); 

// 关闭数据库
mysqli_close($conn);