<?php

/** 
* ==  Vx_Download  == 
*
* 文件功能： 
*		修改相关数据
*
* @author		迷人月色
* @version		0.9 
* @time			2022-04-09
* @QQ			656536055
*
*/ 

// 设置编码
header("Content-type:text/html;charset=utf-8");

// 关闭warning提示
ini_set("display_errors", 0);
error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ALL ^ E_WARNING);

// 加载文件
require_once("../config.php");

// 启动 Session
session_start();


// 判断是否登陆
if (empty($_SESSION['username'])){
	header('location:./login.php');
}


// 获取要修改的序号
$url = $_GET["url"];

// 查询并获取指定序号的内容
$sql = $conn->query("select * from vx_download_share where url= '$url' ");
$arr = mysqli_fetch_array($sql,MYSQLI_ASSOC);

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="./style/layui.css" media="all">
<link rel="stylesheet" href="./style/auto.css" media="all">
<script src="./style/layui.js"></script>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title><?echo BLOG_NAME?> - 修改数据</title>
</head>
<body>
<center><h1 id="edit_title">修改数据</h1></center>

<!-- 开启表单 -->
<div id="edit_box">
<form action="update.php" class="layui-form layui-form-pane"class="layui-form layui-form-pane" style="width: 100%;" method="post">
	<div class="layui-form-item">
	<label class="layui-form-label">文章序号</label>
	<div class="layui-input-block">
	<input type="text" name="url" autocomplete="off" readonly="readonly" style="width:40%" value="<?php echo $arr['url'];?>" placeholder="请输入文章序号" class="layui-input"><span id="edit_notice">&nbsp;&nbsp;此数据不可修改</span></p>
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label">文章标题</label>
	<div class="layui-input-block">
	<input type="text" name="title" autocomplete="off" value="<?php echo $arr['title']; ?>" placeholder="请输入标题" class="layui-input">
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label"><input class="layui-input" style="border: 1px solid;height: 25px;" name="ali_title" value="<?php echo $arr['ali_title']; ?>" id="input_title_box" autocomplete="off"></label>
	<div class="layui-input-block">
	<input type="text" name="ali_share" autocomplete="off" value="<?php echo $arr['ali_share']; ?>" placeholder="请输入阿里云盘分享链接" class="layui-input">
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label">提取密码</label>
	<div class="layui-input-block">
	<input type="text" name="code" autocomplete="off" value="<?php echo $arr['code']; ?>" placeholder="请输入阿里云盘提取码" class="layui-input">
	</div>
	</div>
	<div class="layui-form-item">
	<label class="layui-form-label"><input class="layui-input" style="border: 1px solid;height: 25px;" name="other_title" value="<?php echo $arr['other_title']; ?>" id="input_title_box" autocomplete="off"></label>
	<div class="layui-input-block">
	<input type="text" name="other_share" autocomplete="off" value="<?php echo $arr['other_share']; ?>" placeholder="请输入其他云盘分享链接" class="layui-input">
	</div>
	</div>
	<br/><br/><br/>
	<div class="layui-input-block">
      <button type="submit" id="edit_button" class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
    </div>
</form>
</div>

</body>

</html>